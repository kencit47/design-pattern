package pattern.object;

public class Garage {
	public void open() {
		System.out.println("Garage open");
	}
	
	public void close() {
		System.out.println("Garage close");
	}
}
