package pattern;

import lombok.Setter;
import pattern.command.Command;
import pattern.command.impl.GarageDoorOpenCommand;
import pattern.command.impl.LightOnCommand;
import pattern.object.Garage;
import pattern.object.Light;

public class SimpleRemoteControl {
	@Setter
	Command onCommand;
	
	@Setter
	Command offCommand;
	
	public SimpleRemoteControl() {
		
	}
	
	public void onButtonWasPressed() {
		onCommand.execute();
	}
	
	public void offButtonWasPressed() {
		offCommand.execute();
	}
}

