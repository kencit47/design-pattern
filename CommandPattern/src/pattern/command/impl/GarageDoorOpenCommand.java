package pattern.command.impl;

import pattern.command.Command;
import pattern.object.Garage;

public class GarageDoorOpenCommand implements Command {
	Garage garage;
	
	public GarageDoorOpenCommand(Garage garage) {
		this.garage = garage;
	}
	
	@Override
	public void execute() {
		garage.open();
	}

}
