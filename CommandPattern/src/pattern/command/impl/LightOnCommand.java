package pattern.command.impl;

import pattern.command.Command;
import pattern.object.Light;

public class LightOnCommand implements Command{
	Light light;
	
	public LightOnCommand(Light light) {
		this.light = light;
	}
	
	@Override
	public void execute() {
		light.on();
	}

}

