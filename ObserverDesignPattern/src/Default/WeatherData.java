package Default;

import java.util.Observable;

public class WeatherData extends Observable{
	private float temporature;
	private float humi;
	private float pressure;
	
	public WeatherData() {}
	
	public void measurementsChanged() {
		setChanged();
		notifyObservers();
	}
	
	public void setMesurements(float temp, float humi, float pressure) {
		this.temporature = temp;
		this.humi = humi;
		this.pressure = pressure;
		measurementsChanged();
	}

	public float getTemporature() {
		return temporature;
	}

	public float getHumi() {
		return humi;
	}

	public float getPressure() {
		return pressure;
	}
	
	
}
