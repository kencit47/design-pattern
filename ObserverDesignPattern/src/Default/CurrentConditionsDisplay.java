package Default;

import java.util.Observable;
import java.util.Observer;

public class CurrentConditionsDisplay implements Observer{
	Observable observable;
	private float temperature;
	private float humidity;
	
	public CurrentConditionsDisplay(Observable observable) {
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(o.getClass().isAssignableFrom(WeatherData.class)) {
			WeatherData weatherData = (WeatherData) o;
			this.temperature = weatherData.getTemporature();
			this.humidity = weatherData.getHumi();
			display();
		}
	}
	
	public void display() {
		
	}

}
