package Customize;

public class StockObserver implements Observer {
	public static int observerIDTracker = 0;
	
	private double ibmPrice;
	private double applPrice;
	private double googPrice;
	
	private int observerID;
	private Subject stockGrabber;
	
	public StockObserver(Subject stockGrabber) {
		this.stockGrabber = stockGrabber;
		
		observerID = ++observerIDTracker;
		
		System.out.println("New Observer " + this.observerID);
		
		this.stockGrabber.register(this);
	}

	@Override
	public void update(double ibmPrice, double aaplPrice, double googPrice) {
		this.ibmPrice = ibmPrice;
		this.applPrice = aaplPrice;
		this.googPrice = googPrice;
		printThePrices();
	}
	
	public void printThePrices() {
		System.out.println("Observer" + "\nIBM " + ibmPrice + "\nAAPL " + applPrice + "\nGOOG " + googPrice);
	}
}
