package Customize;

public class GrabStocks {

	public static void main(String[] args) {
		StockGrabber stockGrabber = new StockGrabber();
		new StockObserver(stockGrabber);
		new StockTwoObserver(stockGrabber);
		
		stockGrabber.setIBMPrice(197.00);
		stockGrabber.setAAPLPrice(677.00);
		stockGrabber.setGOOGPrice(572.00);
	}

}
