package Customize;

public class StockTwoObserver implements Observer {
	private double ibmPrice;
	private double aaplPrice;
	private double googPrice;
	
	private int observerID;
	private Subject subject;
	
	public StockTwoObserver(Subject newSubject) {
		this.subject = newSubject;
		observerID = ++StockObserver.observerIDTracker;
		System.out.println("New Observer " + this.observerID);
		this.subject.register(this);
	}
	
	@Override
	public void update(double newibmPrice, double newaaplPrice, double newgoogPrice) {
		this.ibmPrice = newibmPrice;
		this.aaplPrice = newaaplPrice;
		this.googPrice = newgoogPrice;
		printThePrices();
	}
	
	public void printThePrices() {
		System.out.println("Observer" + "\nIBM " + ibmPrice + "\nAAPL " + aaplPrice + "\nGOOG " + googPrice);
	}

}
