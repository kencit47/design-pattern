
public class TestRobotBuilder {
	
	public static void main(String[] args) {
		RobotBuilder robotBuilder = new OldRobotBuilder();
		RobotEngineer robotEngineer = new RobotEngineer(robotBuilder);
		
		robotEngineer.makeRobot();
		Robot firstRobot = robotEngineer.getRobot();
		System.out.println(firstRobot.getRobotTorso());
	}
	
}
