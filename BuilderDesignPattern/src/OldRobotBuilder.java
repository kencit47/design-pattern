
public class OldRobotBuilder implements RobotBuilder{
	private Robot robot;
	
	public OldRobotBuilder() {
		this.robot = new Robot();
	}
	
	@Override
	public RobotBuilder buildRobotHead() {
		robot.setRobotHead("Tin Head");
		return this;
	}

	@Override
	public RobotBuilder buildRobotTorso() {
		robot.setRobotTorso("Tin Torso");
		return this;
	}

	@Override
	public RobotBuilder buildRobotArms() {
		robot.setRobotArms("Tin Arms");
		return this;
	}

	@Override
	public RobotBuilder buildRobotLegs() {
		robot.setRobotArms("Arms");
		return this;
	}
	
	@Override
	public Robot getRobot() {
		return robot;
	}

}
