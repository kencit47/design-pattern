import decorator.Beverage;
import drinks.Coffe;
import topping.Mocha;
import topping.Whip;

public class Main {

	public static void main(String[] args) {
		Beverage beverage = new Coffe(Beverage.BIG);
		System.out.println(beverage.getDescription() + " - " + beverage.cost());
		
		beverage = new Mocha(beverage);
		
		System.out.println(beverage.getDescription() + " - " + beverage.cost());
		
		beverage = new Whip(beverage);
		
		System.out.println(beverage.getDescription() + " - " + beverage.cost());
	}
}
