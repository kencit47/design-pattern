package decorator;

public abstract class Beverage {
	
	 public static final String SMALL = "small";
	 public static final String BIG = "big";
	
	protected String description = "Unknow Beverage";
	
	public String getDescription() {
		return description;
	}
	
	public abstract double cost();
	
	public abstract String getSize();
}
