package topping;

import decorator.Beverage;

public abstract class ToppingFather extends Beverage{
	public abstract String getDescription();
}
