package topping;

import decorator.Beverage;

public class Whip extends ToppingFather{
	Beverage beverage;
	
	public Whip(Beverage beverage) {
		this.beverage = beverage;
	}
	@Override
	public String getDescription() {
		return beverage.getDescription() + "Whip";
	}

	@Override
	public double cost() {
		if(getSize() == Beverage.SMALL) {
			return beverage.cost() + 4000 + 300;
		} else {
			return beverage.cost() + 4000 + 400;
		}
	}
	@Override
	public String getSize() {
		return beverage.getSize();
	}

}
