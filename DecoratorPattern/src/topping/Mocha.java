package topping;

import decorator.Beverage;

public class Mocha extends ToppingFather {
	Beverage beverage;
	
	public Mocha(Beverage beverage) {
		this.beverage = beverage;
	}
	@Override
	public String getDescription() {
		return beverage.getDescription() + " Mocha";
	}

	@Override
	public double cost() {
		if(getSize() == Beverage.SMALL) {
			return beverage.cost() + 3000 + 100;
		} else {
			return beverage.cost() + 3000 + 200;
		}
	}
	@Override
	public String getSize() {
		return beverage.getSize();
	}

}
