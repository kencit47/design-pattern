package drinks;

import decorator.Beverage;

public class Capuchino extends Beverage {
	String size;
	
	public Capuchino(String size) {
		description = "Capuchino";
		this.size = size;
	}
	@Override
	public double cost() {
		return 15000;
	}
	@Override
	public String getSize() {
		return size;
	}

}
