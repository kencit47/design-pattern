package drinks;

import decorator.Beverage;

public class Coffe extends Beverage{
	String size;
	public Coffe(String size) {
		description = "Coffe";
		this.size = size;
	}
	@Override
	public double cost() {
		return 20000;
	}
	@Override
	public String getSize() {
		return size;
	}

}
