package example.newone;

import example.newone.impl.lv1.NYPizzaIngredientFactory;
import example.newone.itface.lv1.PizzaIngredientFactory;
import example.newone.product.lv1.Pizza;
import example.newone.product.lv2.CheesePizza;

public class PizzaStore {
	Pizza pizza = null;
	PizzaIngredientFactory pizzaIngredientFactory = new NYPizzaIngredientFactory();
	
	protected Pizza createPizza(String item) {
		if(item.equals("cheese")) {
			pizza = new CheesePizza(pizzaIngredientFactory);
			pizza.setName("Cheese pizza");
		}
		
		return pizza;
	}
}
