package example.newone.itface.lv1;

import example.newone.itface.lv2.Cheese;
import example.newone.itface.lv2.Clams;
import example.newone.itface.lv2.Dough;
import example.newone.itface.lv2.Pepperoni;
import example.newone.itface.lv2.Sauce;
import example.newone.itface.lv2.Vaggies;

public interface PizzaIngredientFactory {
	public Dough createDough();
	public Sauce createSauce();
	public Cheese createCheese();
	public Vaggies[] createVaggies();
	public Pepperoni createPepperoni();
	public Clams createClams();
}
