package example.newone.product.lv1;

import example.newone.itface.lv2.Cheese;
import example.newone.itface.lv2.Clams;
import example.newone.itface.lv2.Dough;
import example.newone.itface.lv2.Pepperoni;
import example.newone.itface.lv2.Sauce;
import example.newone.itface.lv2.Vaggies;
import lombok.Getter;
import lombok.Setter;

public abstract class Pizza {
	@Getter
	@Setter
	protected String name;
	protected Dough dough;
	protected Sauce sauce;
	protected Vaggies vaggies[];
	protected Cheese cheese;
	protected Pepperoni pepperoni;
	protected Clams clams;
	
	protected abstract void prepare();
	
	void bake() {
		System.out.println("Bake pizza");
	}
	
	void cut() {
		System.out.println("Cut pizza");
	}
	
	void box() {
		System.out.println("Box pizza");
	}
}
