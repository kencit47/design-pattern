package example.newone.impl.lv1;

import example.newone.impl.lv2.cheese.ReggianoCheese;
import example.newone.impl.lv2.clams.FreshClams;
import example.newone.impl.lv2.dough.ThinCrustDough;
import example.newone.impl.lv2.pepperoni.SlicedPepperoni;
import example.newone.impl.lv2.sauce.MarinaraSauce;
import example.newone.impl.lv2.vaggies.Garlic;
import example.newone.impl.lv2.vaggies.Mushroom;
import example.newone.impl.lv2.vaggies.Onion;
import example.newone.impl.lv2.vaggies.RedPepper;
import example.newone.itface.lv1.PizzaIngredientFactory;
import example.newone.itface.lv2.Cheese;
import example.newone.itface.lv2.Clams;
import example.newone.itface.lv2.Dough;
import example.newone.itface.lv2.Pepperoni;
import example.newone.itface.lv2.Sauce;
import example.newone.itface.lv2.Vaggies;

public class NYPizzaIngredientFactory implements PizzaIngredientFactory {

	@Override
	public Dough createDough() {
		return new ThinCrustDough();
	}

	@Override
	public Sauce createSauce() {
		return new MarinaraSauce();
	}

	@Override
	public Cheese createCheese() {
		return new ReggianoCheese();
	}

	@Override
	public Vaggies[] createVaggies() {
		Vaggies[] vaggies = {new Garlic(), new Onion(), new Mushroom(), new RedPepper()};
		return vaggies;
	}

	@Override
	public Pepperoni createPepperoni() {
		return new SlicedPepperoni();
	}

	@Override
	public Clams createClams() {
		return new FreshClams();
	}

}
