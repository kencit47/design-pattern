package example.newone.impl.lv1;

import example.newone.impl.lv2.cheese.ChicagoCheese;
import example.newone.impl.lv2.clams.FronzenClams;
import example.newone.impl.lv2.dough.ThickCrustDough;
import example.newone.impl.lv2.pepperoni.ChicagoPepperoni;
import example.newone.impl.lv2.sauce.ChicagoSauce;
import example.newone.impl.lv2.vaggies.Garlic;
import example.newone.impl.lv2.vaggies.Mushroom;
import example.newone.impl.lv2.vaggies.Onion;
import example.newone.impl.lv2.vaggies.RedPepper;
import example.newone.itface.lv1.PizzaIngredientFactory;
import example.newone.itface.lv2.Cheese;
import example.newone.itface.lv2.Clams;
import example.newone.itface.lv2.Dough;
import example.newone.itface.lv2.Pepperoni;
import example.newone.itface.lv2.Sauce;
import example.newone.itface.lv2.Vaggies;

public class ChicagoPizzaIngredientFactory implements PizzaIngredientFactory {

	@Override
	public Dough createDough() {
		return new ThickCrustDough();
	}

	@Override
	public Sauce createSauce() {
		return new ChicagoSauce();
	}

	@Override
	public Cheese createCheese() {
		return new ChicagoCheese();
	}

	@Override
	public Vaggies[] createVaggies() {
		Vaggies[] vaggies = {new Garlic(), new Onion(), new Mushroom(), new RedPepper()};
		return vaggies;
	}

	@Override
	public Pepperoni createPepperoni() {
		return new ChicagoPepperoni();
	}

	@Override
	public Clams createClams() {
		return new FronzenClams();
	}

}
