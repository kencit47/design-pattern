package example.old.creator;

import example.old.concreteProduct.BigUFOEnemyShip;
import example.old.concreteProduct.RocketEnemyShip;
import example.old.concreteProduct.UFOEnemyShip;
import example.old.product.EnemyShip;

public class EnemyShipFactory {
	public static EnemyShip makeEnemyShip(String newShipType) {
		EnemyShip newShip = null;
		
		if(newShipType.equalsIgnoreCase("u")) {
			newShip = new UFOEnemyShip();
		} else 
		if(newShipType.equalsIgnoreCase("r")) {
			newShip = new RocketEnemyShip();
		}else
		if(newShipType.equalsIgnoreCase("b")) {
			newShip = new BigUFOEnemyShip();
		}
		return newShip;
	}
}
