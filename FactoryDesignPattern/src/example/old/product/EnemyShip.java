package example.old.product;

import lombok.Data;

@Data
public abstract class EnemyShip {
	private String name;
	private double amtDamage;
	
	public void followHeroShip() {
		System.out.println(getName() + " follow hero ship");
	}
	
	public void displayEnemyShip() {
		System.out.println(getName() + " is on screen");
	}
}
