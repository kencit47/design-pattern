package example.old;
import java.util.Scanner;

import example.old.creator.EnemyShipFactory;
import example.old.product.EnemyShip;

public class EnemyTesting {
	public static void main(String[] args) {
		EnemyShip theEnemy;
		Scanner scan = new Scanner(System.in);
		
		System.out.println("What ship do you want to create? U/R/B");
		
		theEnemy = EnemyShipFactory.makeEnemyShip(scan.nextLine());
		
		doStuffEnemy(theEnemy);
	}
	
	public static void doStuffEnemy(EnemyShip enemyShip) {
		enemyShip.displayEnemyShip();
		enemyShip.followHeroShip();
	}
}
