package example.old.concreteProduct;

import example.old.product.EnemyShip;

public class RocketEnemyShip extends EnemyShip {
	
	public RocketEnemyShip() {
		setName("UFO Enemy Ship");
		setAmtDamage(20.0);
	}
}
