package example.old.concreteProduct;

import example.old.product.EnemyShip;

public class UFOEnemyShip extends EnemyShip {
	
	public UFOEnemyShip() {
		setName("UFO Enemy Ship");
		setAmtDamage(20.0);
	}
}
