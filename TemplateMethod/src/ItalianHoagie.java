
public class ItalianHoagie extends Hoagie{

	String[] meatUsed = {"Salami", "Pepperoni", "Capicola Ham"};
	String[] cheeseUsed = {"Provolone"};
	String[] veggiesUsed = {"Lettuce", "Tomatoes", "Onions", "Sweet Peppers"};
	String[] condimentsUsed = {"Oil", "Vinegar"};
	
	@Override
	boolean customerWantMeat() {return false;}
	@Override
	boolean customerWantCheese() {return false;}
	
	@Override
	void addMeat() {}

	@Override
	void addVegetables() {}

	@Override
	void addCheese() {
		System.out.println("Adding the Veggies: ");
		for(String veggies : veggiesUsed) {
			System.out.println(veggies + " ");
		}		
	}

	@Override
	void addCondiments() {
		System.out.println("Adding the Condiments: ");
		for(String condiment : condimentsUsed) {
			System.out.println(condiment + " ");
		}		
	}

}


