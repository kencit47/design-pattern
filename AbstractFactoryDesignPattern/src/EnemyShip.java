
public abstract class EnemyShip {

	public abstract void makeShip();

	public abstract void displayShip();

	public abstract void followHeroShip();

	public EnemyShip makeEnemyShip(String type) {
		EnemyShip newShip = null;
		
		if(type.equalsIgnoreCase("u")) {
			newShip = new UFOEnemyShip();
		} else 
		if(type.equalsIgnoreCase("r")) {
			newShip = new RocketEnemyShip();
		}else
		if(type.equalsIgnoreCase("b")) {
			newShip = new BigUFOEnemyShip();
		}
		return newShip;
	}

}
