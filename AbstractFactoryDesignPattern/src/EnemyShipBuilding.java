
public abstract class EnemyShipBuilding {
	protected abstract EnemyShip makeEnemyShip(String type);
	
	public EnemyShip orderEnemyShip(String type) {
		EnemyShip theEnemyShip = makeEnemyShip(type);
		
		theEnemyShip.makeShip();
		theEnemyShip.displayShip();
		theEnemyShip.followHeroShip();
		
		return theEnemyShip;
	}
}
